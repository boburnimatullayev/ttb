import styles from "./styles.module.scss";
import Link from "next/link";

export default function Header() {
  return (
    <header className={styles.header}>
      <Link href="/about">About</Link>
    </header>
  );
}
